using Iterators

const n_players = 3
const grid_size = 0.01

function main()
    p = 0:grid_size:1

    payoffs_list = getPayoffsList(p)

    nash_eq = getNashEq(p, payoffs_list)

    for i in nash_eq
        println(i)
    end
end


function getPayoffs(p)
    # p[1] = P(1 plays A), p[2] = P(2 plays A), p[3] = P(3 plays R)

    payoffs = ((1-p[1]) .* ((1-p[3]) .* [3,3,2] + p[3] .* [0,0,0])
               + p[1] * (p[2] .* [1,1,1]
                         + (1-p[2]) .* ((1-p[3]) .* [4,4,0] + p[3] .* [0,0,1])))

    return payoffs
end



function getPayoffsList(p)
    grid_length = length(p)
    indices = product([1:grid_length for j in 1:n_players]...)


    # make list with length n_players, containing
    # a grid_length x grid_length ... (n_players times) ... x grid_length
    # array in each entry
    
    payoffs_list = [zeros([grid_length for j=1:n_players]...) for i=1:n_players]

    for index in indices
        payoffs = getPayoffs([p[i] for i in index])

        for player=1:n_players
            payoffs_list[player][index...] = payoffs[player]
        end
    end

    return payoffs_list
end

        
function getNashEq(p, payoffs_list)
    error_tol = 0.000001
    grid_length = length(p)
    nash_eq = Array{Array{Float64,1}, 1}()
    indices = product([1:grid_length for j in 1:n_players]...)

    function testNash(index)
        potential_nash = convert(Array{Bool}, zeros(n_players))        
        for player=1:n_players
            # player_index replaces the current player's dimension with
            # a colon so we can slice along the current player's choices
            player_index = [i == player ? Colon() : index[i] for i in 1:n_players]

            payoff = payoffs_list[player][index...]
            best_alternative = maximum(getindex(payoffs_list[player], player_index...))

            if abs(payoff - best_alternative) < error_tol
                potential_nash[player] = true
            else
                break
            end
        end
        if all(potential_nash)
            return [p[i] for i in index]
        end
    end

    nash_eq = pmap(testNash, indices)
    return vcat(filter(x->x!=nothing,nash_eq))
end


            
          
    
    
